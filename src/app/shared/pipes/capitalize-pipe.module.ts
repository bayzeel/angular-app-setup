import { NgModule } from '@angular/core';
import { CapitalizePipe } from '@shared/pipes/capitalize.pipe';

@NgModule({
  declarations: [
    CapitalizePipe
  ],
  exports: [
    CapitalizePipe
  ]
})
export class CapitalizePipeModule { }
