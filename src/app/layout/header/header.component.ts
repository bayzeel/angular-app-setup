import { Component, OnInit } from '@angular/core';
import { AuthService } from '@data/services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public currentUser$;
  private _loginSubscription: Subscription;

  constructor(
    private readonly _authService: AuthService
  ) { }

  public ngOnInit(): void {
    this.currentUser$ = this._authService.currentUser;
  }

  public login(): void {
    if (this._loginSubscription) {
      this._loginSubscription.unsubscribe();
    }

    this._loginSubscription = this._authService.login().subscribe();
  }

  public logout(): void {
    this._authService.logout();
  }
}
