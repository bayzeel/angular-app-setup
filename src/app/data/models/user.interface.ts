import { data } from '../';

export interface UserInterface {
  id: string;
  username: string;
  roles: data.enums.RolesEnum[];
}
