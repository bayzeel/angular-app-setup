import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env';
import { BehaviorSubject, EMPTY, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { data } from '../';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly _currentUser$: BehaviorSubject<data.models.UserInterface> = new BehaviorSubject<data.models.UserInterface>(undefined);

  constructor(
    private readonly _httpClient: HttpClient
  ) { }

  public login(): Observable<void> {
    const url = `${environment.apiUrl}auth`;

    return this._httpClient.get<data.models.UserInterface>(url).pipe(
      switchMap((user: data.models.UserInterface) => {
        this._currentUser$.next(user);

        return EMPTY;
      })
    );
  }

  public logout(): void {
    this._currentUser$.next(undefined);
  }

  get currentUser(): Observable<data.models.UserInterface> {
    return this._currentUser$.asObservable();
  }
}
