import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CapitalizePipeModule } from '@shared/pipes/capitalize-pipe.module';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';

@NgModule({
  declarations: [
    AdminComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    CapitalizePipeModule
  ]
})
export class AdminModule { }
