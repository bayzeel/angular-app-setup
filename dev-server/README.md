Server documentation: https://www.npmjs.com/package/json-server

To install json-server use command `npm install -g json-server`.

To run server use command `json-server --watch db.json` from the `dev-server` folder.

After changing data in the `db.json` file (by POST, PUT, DELETE requests) you can copy data from the `db-keep.json`.

If you need to add a new data to the server, add it both to the `db.json` and `db-keep.json` files.
